const express = require ("express");
const http = require ("http");
const socketIo = require ("socket.io");
const axios = require ("axios");
const fs = require("fs");
const port = process.env.PORT || 4003;
const app = express ();
app.use (express.static("public"));
const server = http.createServer (app);
const io = socketIo (server);
const ioDisplay = io.of('/display');

const debugMode = true;

const STATE_WAITING_ROOM = "WAITING_ROOM";
const STATE_GAME_START = "GAME_START"; //allow time for animations
const STATE_SEND_QUESTION = "SEND_QUESTION"; //broadcast question to both rooms
const STATE_WAIT_FOR_ANSWERS = "WAITING_ANSWERS"; //10s timer or if all answers are in
const STATE_REVIEW_ANSWER = "REVIEW_ANSWER"; //allow time to animate point totals
var CURRENT_STATE = STATE_WAITING_ROOM;

const ROUND_FIRST = "FIRST_ROUND"; //10 questions
const ROUND_HIGH_SCORING = "HIGH_SCORING_ROUND"; //10 questions
const ROUND_FINAL = "FINAL ROUND"; //5 questions
const ROUND_TIEBREAKER = "TIEBREAKER"; //1 question
var CURRENT_ROUND = ROUND_FIRST;

const DURATION_WAIT_START_ANIMATIONS = 5000;//5000ms
const DURATION_WAIT_ANSWERS = 10000;//10, 000 ms 
const DURATION_REVIEW_ANSWER = 5000;//5000ms
const DURATION_NONE = 1;
var WAIT_TIMER;


var MAX_PLAYER_COUNT = 4;
var totalPlayerCount = -1;
var captainFound = false;

var currentQuestion = 0;
var receivedAnswers = 0;

var gameMode;
var players = [];
var displaySockets = [];
var questionsData = [];

var displayMessageQueue = [];

function getTriviaData () {
	try {
		fs.readFile("data/wwtbam_json_file.json", function(err, buf) {
		  questionsData = JSON.parse(buf).questions;
		  shuffleArray (questionsData);
		});
	} catch (error) {
		if (debugMode)console.error(`error: ${error.code}`);
	}
}

getTriviaData(null);


//TODO - Add more player validation
io.of('/').on ("connection", socket => {
	if (debugMode)console.log ("new client connected");

	socket.isSocketRegistered = false;
	socket.registeredId = -1;

	socket.on('player-register', function (msg) {
		if (!socket.isSocketRegistered && socket.registeredId == -1)
		{
			if (debugMode)console.log("Player " + msg + " has joined.");
			socket.registeredId = players.length;
			var p = new Player (players.length, msg, socket);
			p.avatar = socket.registeredId;
			players.push(p);
			socket.isSocketRegistered = true;
			if (!captainFound)
			{
				//first player to connect, make them the captain
				captainFound = true;
				p.isCaptain = true;
				socket.emit ("set-buttons-captain", {"header":"How many players?", "A":"1", "B":"2", "C":"3", "D":"4"});
			}
			if (checkPlayerCount())
			{
				io.sockets.emit('info', players.length);
				setGameState (STATE_GAME_START);
			}
			
			var playerDataToSend = copyPlayer (p);
			playerDataToSend.socket = "";
			ioDisplay.binary(false).emit ("player-added", playerDataToSend);
		}
		else
		{
			if (debugMode)console.log("Player " + players[socket.registeredId].name + " is already registered to " + socket.registeredId);
		}
		
	});

	//can only come from the captain player
	socket.on('set-player-count', function (msg)
	{
		var amount;
		switch(msg)
		{
			case "A":
				amount = 1;
				break;
			case "B" :
				amount = 2;
				break;
			case "C" :
				amount = 3;
				break;
			case "D" :
				amount = 4;
				break;
			default :
				amount = 1;
				break;
		}
		if (amount < MAX_PLAYER_COUNT)
		{
			totalPlayerCount = amount;
			io.sockets.emit('info', totalPlayerCount);
		}
		if (checkPlayerCount())
		{
			io.sockets.emit('info', players.length);
			setGameState (STATE_GAME_START);
		}
	});

	socket.on('question-response', function (msg)
	{
		players[socket.registeredId].currentAnswer = msg;
		receivedAnswers += 1;

		if (receivedAnswers == totalPlayerCount)//everyone has rung in their answers
		{
			setGameState (STATE_REVIEW_ANSWER);
		}
	});

	socket.on ("disconnect", () => {
		if (debugMode)console.log ("Client disconnected");
	});
});

//the display is meant to receive state. The display does not send commands to the server
ioDisplay.on ("connection", socket => {
	if (debugMode)console.log ("new display client connected");
	//on connection, send all player data (score, avatar, etc) and current state (game state, current question)
	ioDisplay.binary(false).emit ("state-change", CURRENT_STATE);
	for (var i = 0; i < players.length; i++)
	{
		var toSend = copyPlayer (players[i]);
		toSend.socket = "";
		ioDisplay.binary(false).emit ("player-added", toSend);
	}
	if (CURRENT_STATE != STATE_WAITING_ROOM && CURRENT_STATE != STATE_GAME_START)
	{
		ioDisplay.binary(false).emit('question', questionsData[currentQuestion-1]);
	}

	socket.on ("disconnect", () => {
		if (debugMode)console.log ("Client disconnected");
	});
});

function Player (id, name, socket) {
	this.name = name;
	this.socket = socket;
	this.score = 0;
	this.id = id; 
	this.image; //implement last - image drawn by player
	this.avatar; //eventually allow the player to choose avatar
	this.isCaptain = false;//the first person to connect will have control of the flow
	this.currentAnswer;
}

function copyPlayer (old)
{
	var player = new Player ("", "", "");
	player.name = old.name;
	player.socket = old.socket;
	player.score = old.score;
	player.id = old.id; 
	player.image = old.image;
	player.avatar = old.avatar; 
	player.isCaptain = old.isCaptain;
	player.currentAnswer = old.currentAnswer;

	return player;
}

function checkPlayerCount ()
{
	if (totalPlayerCount != -1)
	{
		if (players.length == totalPlayerCount)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function clearAllAnswers ()
{
	for (var i = 0; i < players.length; i++)
	{
		players[i].currentAnswer = "";
	}
}

function reviewAnswers (round)
{
	var value;
	if (round == ROUND_FIRST)
	{
		value = 200;
	}
	else if (round == ROUND_HIGH_SCORING)
	{
		value = 500;
	}
	else if (round == ROUND_TIEBREAKER)
	{
		value = 100;
	}
	else if (round == ROUND_FINAL)
	{
		value = 1000;
	}
	var answer = questionsData[currentQuestion-1].answer;
	for (var i = 0; i < players.length; i++)
	{
		if (players[i].currentAnswer == answer)
		{
			players[i].score += value;
			
		}
		if ((round == ROUND_FIRST || round == ROUND_HIGHSCORING) && players[i].currentAnswer != answer)
		{
			players[i].score -= 200;
		}
		console.log ("Correct Answer : " + answer + " " + players[i].name + "'s answer : " + players[i].currentAnswer + " " + players[i].score);
		players[i].socket.emit ("score", players[i].score);
		ioDisplay.binary(false).emit ("score", {id:players[i].id, score:players[i].score});
		console.log (questionsData[currentQuestion-1][answer]);
		players[i].socket.emit ("answer", questionsData[currentQuestion-1][answer]);
		ioDisplay.binary(false).emit ("answer", questionsData[currentQuestion-1][answer]);
	}
}


function setGameState (newState)
{
	switch (newState) {
		case STATE_GAME_START :
			if (checkPlayerCount())
			{
				CURRENT_STATE = STATE_GAME_START;
				io.sockets.emit('state-change', STATE_GAME_START);
				ioDisplay.binary(false).emit ('state-change', STATE_GAME_START);
				setTimeout (function () {
					setGameState (STATE_SEND_QUESTION);
				},DURATION_WAIT_START_ANIMATIONS);
			}
			break;


		case STATE_SEND_QUESTION :
			CURRENT_STATE = STATE_SEND_QUESTION;
			io.sockets.emit('state-change', STATE_SEND_QUESTION);
			ioDisplay.binary(false).emit ('state-change', STATE_SEND_QUESTION);
			var question = getNextQuestion()
			io.sockets.emit('question', question);
			ioDisplay.binary(false).emit('question', question);

			setTimeout (function () {
				setGameState (STATE_WAIT_FOR_ANSWERS);
			},DURATION_NONE);
			break;


		case STATE_WAIT_FOR_ANSWERS :
			CURRENT_STATE = STATE_WAIT_FOR_ANSWERS;
			io.sockets.emit('state-change', STATE_WAIT_FOR_ANSWERS);
			ioDisplay.binary(false).emit ('state-change', STATE_WAIT_FOR_ANSWERS);
			break;


		case STATE_REVIEW_ANSWER :
			CURRENT_STATE = STATE_REVIEW_ANSWER;
			io.sockets.emit('state-change', STATE_REVIEW_ANSWER);
			ioDisplay.binary(false).emit ('state-change', STATE_REVIEW_ANSWER);
			reviewAnswers ();
			receivedAnswers = 0;
			clearAllAnswers ();

			if (currentQuestion == 10)
			{
				//change to the high scoring round
			}

			setTimeout (function () {
				setGameState (STATE_SEND_QUESTION);
			},DURATION_REVIEW_ANSWER);
			break;


		default :
			break;
	}
}

function getNextQuestion ()
{
	var question = questionsData[currentQuestion];
	currentQuestion += 1;

	return question;
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

server.listen (port, () => console.log (`listening on port ${port}`));