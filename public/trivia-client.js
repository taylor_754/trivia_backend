//you already included a reference to the socket.io library in index.html so now you have access to it
//make a socket by declaring var socket which will make a socket connection to localhost:4000
var socket = io.connect("http://10.250.202.92:4003");

var responseCaptainPlayerCount = false;
var score = 0;

socket.on ("connect", function (){
	console.log ("connected");
});

socket.on ("set-buttons-captain", function (data){
	console.log (data);
	responseCaptainPlayerCount = true;
	setButtons (data);
});

socket.on ("state-change", function (data){
	console.log ("STATE CHANGE : " + data);
	if (data == "GAME_START")
	{
		setButtons({question:"Get Ready!", A:"", B:"", C:"", D:""});
	}
});

socket.on ("info", function (data){
	console.log ("INFO " + data);
});

socket.on ("question", function (data){
	console.log (data);
	setButtons(data);
	$("#correct-answer").hide(333);
});

socket.on ("answer", function (data){
	console.log ("The correct answer was : " + data);
	$("#correct-answer").show();
	$("#correct-answer").html ("The correct answer was : " + data);
})

socket.on ("score", function (data) {
	console.log ("score is :" + data);
	score = data;
	$("#score").html("Score : " + score);
})

function registerPlayer ()
{
	var name = document.getElementById("player-name").value;
	console.log (name);
	socket.emit ('player-register', name);
	$("#name-registration-section").hide(333);
	$("#registered-confirmation").show(333);
	setTimeout (function () {
		$("#registered-confirmation").hide(333);
	}, 3000);
}

function sendResponse (response)
{
	var responseType = (responseCaptainPlayerCount) ? "set-player-count" : "question-response";
	console.log (response);
	socket.emit (responseType, response);
	if (responseCaptainPlayerCount) responseCaptainPlayerCount = false;
}

function setButtons (data)
{
	if (data == "")
	{
		console.log ("data is blank");
		data = {question:"", A:"", B:"", C:"", D:""};
	}
	if (responseCaptainPlayerCount)
	{
		document.getElementById ('buttons-title').innerHTML = data["header"];
	}
	else
	{
		document.getElementById ('buttons-title').innerHTML = data["question"];
	}
	$('#response-A').html(data["A"]);
	$('#response-B').html(data["B"]);
	$('#response-C').html(data["C"]);
	$('#response-D').html(data["D"]);
}

document.getElementById('response-A').addEventListener("click", function() {
	//if server is expecting a response and an option is available
  sendResponse("A");
});

document.getElementById('response-B').addEventListener("click", function() {
  sendResponse("B");
});

document.getElementById('response-C').addEventListener("click", function() {
  sendResponse("C");
});

document.getElementById('response-D').addEventListener("click", function() {
  sendResponse("D");
});