//you already included a reference to the socket.io library in index.html so now you have access to it
//make a socket by declaring var socket which will make a socket connection to localhost:4000
var socket = io.connect("http://10.250.202.92:4003/display");

var players = [];
const STATE_WAITING_ROOM = "WAITING_ROOM";
const STATE_GAME_START = "GAME_START"; //allow time for animations
const STATE_SEND_QUESTION = "SEND_QUESTION"; //broadcast question to both rooms
const STATE_WAIT_FOR_ANSWERS = "WAITING_ANSWERS"; //10s timer or if all answers are in
const STATE_REVIEW_ANSWER = "REVIEW_ANSWER"; //allow time to animate point totals
var CURRENT_STATE = STATE_WAITING_ROOM;

$(".player").hide();

socket.on ("connect", function (){
	console.log ("connected");
});
socket.on ("state-change", function (data){
	console.log ("STATE CHANGE : " + data);

	switch (data) {
		case STATE_GAME_START :
			$("#status-message-box").html ("Get Ready!");
			$("#message-box").html ("");
			break;


		case STATE_SEND_QUESTION :
			
			break;


		case STATE_WAIT_FOR_ANSWERS :
			
			break;


		case STATE_REVIEW_ANSWER :
			
			break;


		default :
			break;
	}
});

socket.on ("player-added", function (data){
	console.log (data);
	players.push (data);
	var currentPlayer = players.length;
	$("#player-"+currentPlayer + " .player-name").html (data.name);
	$("#player-"+currentPlayer).fadeIn();
	$("#message-box").html (data.name + " has joined the game!");
});

socket.on ("info", function (data){
	console.log ("INFO " + data);
});

socket.on ("question", function (data){
	console.log (data);
	$("#status-message-box").html (data.question);
	$("#message-box").hide(333);
	$("#message-box").html ("");
});

socket.on ("answer", function (data){
	console.log ("The correct answer was : " + data);
	$("#message-box").html ("The correct answer was : " + data);
	$("#message-box").show(333);
});

socket.on ("score", function (data) {
	console.log (data);
	console.log ("#player-"+data.id+1 + " .player-score");
	players[data.id].score = data.score;
	$("#player-"+(data.id+1) + " .player-score").html(data.score);
});